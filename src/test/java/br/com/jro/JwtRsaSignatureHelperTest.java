package br.com.jro;

import static org.junit.Assert.assertEquals;

import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;
import org.junit.Before;
import org.junit.Test;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.InvalidKeyException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.HashMap;

/**
 * Unit test for simple App.
 */
public class JwtRsaSignatureHelperTest {

    private String nonEncryptedPrivateKey = null;
    private String encryptedPrivateKey = null;
    private final String encryptionPassphase = "123456";

    @Before
    public void setup() throws IOException {
        nonEncryptedPrivateKey = new String(Files.readAllBytes(Paths.get("src/test/resources/certs/private.key.pem")));
        encryptedPrivateKey = new String(
                Files.readAllBytes(Paths.get("src/test/resources/certs/encrypted.private.key.pem")));
    }

    // #region
    @Test
    public void SigningJwtWithNoExtraHeaderAndWithNonEncryptedPrivateKeyTest()
            throws ExpiredCertificateException, GeneralSecurityException, IOException, InvalidKeyException,
            OperatorCreationException, PKCSException {

        HashMap<String, Object> payload = new HashMap<String,Object>();
        payload.put("sub", "mr.x@contoso.com");
        payload.put("exp", 1300819380);

        String signedJwt = JwtRsaSignatureHelper.CreateSignedJwt(payload, nonEncryptedPrivateKey,
                SignatureAlgorithm.RS256, null, null);

        String expectedSignedJwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.kBMxNItSf7DEWOBUeeySChgzViSY9ynNdDLRYAVnZwtwC7UPxQyD1P3x6UJZ6QCwchHqZznNwccE5W9UhHkV4cr-JSUcakU7fH_SWJJ5r9hDMv5YCz_mpVKNayE8F49XfvNReywLHVRwTRlDLBrlyBzTh963MhAyQHWBjHBzJ3pBLYy_IKcfjmHqvZz2emOBMWve9ECQ-aXgNqyfNaC294vG3scr-gZgMjfZ8nyT99-sHJ95ithAnkW7qMbAgV6wc-YKi0vjXEHhxPHo7iu7Hlzts370iuthyyG34Ne8UqEyRVIg_aBXBbJ6YeBn4CIy5-I8LwkpP0CTKARrqImmVg";
        assertEquals(expectedSignedJwt, signedJwt);
    }

    @Test
    public void SigningJwtWithNoExtraHeaderAndWithEncryptedPrivateKeyTest() throws ExpiredCertificateException,
            GeneralSecurityException, IOException, InvalidKeyException, OperatorCreationException, PKCSException {

        HashMap<String, Object> payload = new HashMap<String,Object>();
        payload.put("sub", "mr.x@contoso.com");
        payload.put("exp", 1300819380);

        String signedJwt =
            JwtRsaSignatureHelper.CreateSignedJwt
            (
                payload,
                encryptedPrivateKey,
                SignatureAlgorithm.RS256,
                null,
                encryptionPassphase
            );

        String expectedSignedJwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.kBMxNItSf7DEWOBUeeySChgzViSY9ynNdDLRYAVnZwtwC7UPxQyD1P3x6UJZ6QCwchHqZznNwccE5W9UhHkV4cr-JSUcakU7fH_SWJJ5r9hDMv5YCz_mpVKNayE8F49XfvNReywLHVRwTRlDLBrlyBzTh963MhAyQHWBjHBzJ3pBLYy_IKcfjmHqvZz2emOBMWve9ECQ-aXgNqyfNaC294vG3scr-gZgMjfZ8nyT99-sHJ95ithAnkW7qMbAgV6wc-YKi0vjXEHhxPHo7iu7Hlzts370iuthyyG34Ne8UqEyRVIg_aBXBbJ6YeBn4CIy5-I8LwkpP0CTKARrqImmVg";
        assertEquals(expectedSignedJwt, signedJwt);
    }

    //#endregion
}
