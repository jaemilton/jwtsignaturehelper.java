package br.com.jro;

public enum AsymmetricAlgorithmTypeEnum
{
    /// <summary>
    ///  When Private key begins with ==> BEGIN RSA PRIVATE KEY
    /// </summary>
    RSAPrivateKey,
    /// <summary>
    /// When Private key begins with ==> BEGIN PRIVATE KEY
    /// </summary>
    Pkcs8PrivateKey,
    /// <summary>
    /// When Private key begins with ==> BEGIN ENCRYPTED PRIVATE KEY
    /// </summary>
    EncryptedPkcs8PrivateKey,
    /// <summary>
    /// When Certificate begins with ==> BEGIN RSA PUBLIC KEY
    /// </summary>
    RSAPublicKey,
    /// <summary>
    /// When Certificate begins with ==> BEGIN PUBLIC KEY
    /// </summary>
    PublicKey,
    /// <summary>
    /// When Certificate begins with ==> BEGIN CERTIFICATE
    /// </summary>
    Certificate
}