package br.com.jro;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCSException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.InvalidKeyException;
import io.jsonwebtoken.security.SignatureException;

public class JwtRsaSignatureHelper {

    public static String CreateSignedJwt(Map<String, Object> payload, String privateKeyText,
            SignatureAlgorithm algorithmType) throws ExpiredCertificateException, GeneralSecurityException, IOException,
            InvalidKeyException, OperatorCreationException, PKCSException {

        return CreateSignedJwt(payload, privateKeyText, algorithmType, null, null);
    }

    public static String CreateSignedJwt(Map<String, Object> payload, String privateKeyText,
            SignatureAlgorithm algorithmType, String secret) throws ExpiredCertificateException,
            GeneralSecurityException, IOException, InvalidKeyException, OperatorCreationException, PKCSException {

        return CreateSignedJwt(payload, privateKeyText, algorithmType, null, secret);
    }

    /**
     * Creates a signed JWT with a private key
     * 
     * @param payload
     * @param privateKeyText Expects the PEM file content Is is prepared to receive
     *                       To summarize each PEM labels accepted to sign a Json:
     *                       “BEGIN RSA PRIVATE KEY” “BEGIN PRIVATE KEY” “BEGIN
     *                       ENCRYPTED PRIVATE KEY”
     * @param algorithmType
     * @param jwtExtraHeader
     * @param secret
     * @return
     * @throws ExpiredCertificateException
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws PKCSException
     * @throws OperatorCreationException
     * @throws InvalidKeyException
     */
    public static String CreateSignedJwt(Map<String, ?> payload, String privateKeyText,
            SignatureAlgorithm algorithmType, Map<String, Object> jwtExtraHeader, String secret)
            throws ExpiredCertificateException, GeneralSecurityException, IOException, InvalidKeyException,
            OperatorCreationException, PKCSException {

        AsymmetricRsaAlgorithmFromTextHelper algorithmFromTextHelper = new AsymmetricRsaAlgorithmFromTextHelper(
                privateKeyText, secret);

        JwtBuilder builder = Jwts.builder();
        JwsHeader header = Jwts.jwsHeader();
        header.setType(JwsHeader.JWT_TYPE);
        if (jwtExtraHeader != null && !jwtExtraHeader.isEmpty()) {
            header.putAll(jwtExtraHeader);
        }

        builder.setHeader((Map<String, Object>) header).setClaims(payload);

        return builder.signWith(algorithmFromTextHelper.getKey(), algorithmType).compact();

    }

    /**
     * Verify a JWT token ao ensure that it`s signed currecty against a public key
     * and returns payload json text of jwt token
     *
     * Expects the PEM file content Is is prepared to receive To summarize each PEM
     * labels: “BEGIN RSA PRIVATE KEY” “BEGIN PRIVATE KEY” “BEGIN ENCRYPTED PRIVATE
     * KEY” “BEGIN RSA PUBLIC KEY” “BEGIN PUBLIC KEY” “BEGIN CERTIFICATE”
     * 
     * @param encodedJwtToken
     * @param publicKeyOrCertificate
     * @param algorithmType
     * @param secret
     * @return
     * @throws ExpiredCertificateException
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws PKCSException
     * @throws OperatorCreationException
     * @throws IllegalArgumentException
     * @throws MalformedJwtException
     * @throws UnsupportedJwtException
     * @throws ExpiredJwtException
     * @throws SignatureException
     */
    public static String GetJsonFromSignedJwt(String encodedJwtToken, String publicKeyOrCertificate, String secret)
            throws ExpiredCertificateException, GeneralSecurityException, IOException, SignatureException,
            ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, IllegalArgumentException,
            OperatorCreationException, PKCSException {

        AsymmetricRsaAlgorithmFromTextHelper algorithmFromTextHelper =
                new AsymmetricRsaAlgorithmFromTextHelper(publicKeyOrCertificate, secret);

        Jws<Claims> jws =
            Jwts.parserBuilder()  // (1)
                .setSigningKey(algorithmFromTextHelper.getKey())         // (2)
                .build()                    // (3)
                .parseClaimsJws(encodedJwtToken); // (4)

        
        return jws.getBody().toString();
    }

}
