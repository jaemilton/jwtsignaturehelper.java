package br.com.jro;


public class ExpiredCertificateException extends Exception {
    public ExpiredCertificateException(String message)
    {
        super(message);
    }
}
