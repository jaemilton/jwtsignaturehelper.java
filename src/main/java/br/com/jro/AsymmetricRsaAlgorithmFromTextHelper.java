package br.com.jro;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import sun.security.util.DerInputStream;
import sun.security.util.DerValue;
import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;
import java.util.Base64;
import java.util.Date;

/**
 * Helper class to get an AsymmetricAlgorithm instance object from a
 * certificate, public key or private key text that starts with:
 * -----BEGIN PUBLIC KEY-----
 * -----BEGIN CERTIFICATE-----
 * -----BEGIN RSA PUBLIC KEY-----
 * -----BEGIN RSA PRIVATE KEY-----
 * -----BEGIN PRIVATE KEY-----
 * -----BEGIN ENCRYPTED PRIVATE KEY-----
 *
 * The Algorithm that can be used as key to Jose.JWT.Encode Jose.JWT.Decode
 */
public class AsymmetricRsaAlgorithmFromTextHelper {

    // static {
    //     Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
    //     Security.addProvider(new BouncyCastleProvider());
    // }

    private final String PublicKeyHeader = "-----BEGIN PUBLIC KEY-----";
    private final String PublicKeyFooter = "-----END PUBLIC KEY-----";
    private final String CertificateHeader = "-----BEGIN CERTIFICATE-----";
    private final String CertificateFooter = "-----END CERTIFICATE-----";
    private final String RSAPublicKeyHeader = "-----BEGIN RSA PUBLIC KEY-----";
    private final String RSAPublicKeyFooter = "-----END RSA PUBLIC KEY-----";

    private final String RSAPrivateKeyHeader = "-----BEGIN RSA PRIVATE KEY-----";
    private final String RSAPrivateKeyFooter = "-----END RSA PRIVATE KEY-----";
    private final String PrivateKeyHeader = "-----BEGIN PRIVATE KEY-----";
    private final String PrivateKeyFooter = "-----END PRIVATE KEY-----";
    private final String PrivateEncryptedKeyHeader = "-----BEGIN ENCRYPTED PRIVATE KEY-----";
    private final String PrivateEncryptedKeyFooter = "-----END ENCRYPTED PRIVATE KEY-----";
    private String secret = null;
    private String _stringBase64 = null;
    private byte[] _sourceBytes = null;



    /**
     * Text of Certificate ou Private key Or Public Key received on constructor
     */
    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String privateKeyOrPublicKeyOrCetificateText;

    /**
     * Header string of Certificate ou Private key Or Public Key received on
     * constructor
     */
    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String headerLabel;

    /**
     * Footer string of Certificate ou Private key Or Public Key received on
     * constructor
     */
    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String footerLabel;

    /**
     * Enum to identify the type of text passed on contructor
     */
    @Setter(AccessLevel.PRIVATE)
    @Getter
    public AsymmetricAlgorithmTypeEnum asymmetricAlgorithmType;

    /**
     * Contructor to identify witch type of certificate, public key or private key
     * is being used
     *
     * @param privateKeyOrCetificateText
     * @param secret
     * @throws InvalidKeyException
     * @throws ExpiredCertificateException
     */
    public AsymmetricRsaAlgorithmFromTextHelper(final String privateKeyOrCetificateText, final String secret)
            throws InvalidKeyException {

        if (StringUtils.isEmpty(privateKeyOrCetificateText)) {
            throw new IllegalArgumentException("privateKeyOrCetificateText can't be null");
        }

        setPrivateKeyOrPublicKeyOrCetificateText(privateKeyOrCetificateText.trim());

        if (CheckHeaderAndFooter(PublicKeyHeader, PublicKeyFooter)) {
            setHeaderLabel(PublicKeyHeader);
            setFooterLabel(PublicKeyFooter);
            setAsymmetricAlgorithmType(AsymmetricAlgorithmTypeEnum.PublicKey);
        } else if (CheckHeaderAndFooter(CertificateHeader, CertificateFooter)) {
            setHeaderLabel(CertificateHeader);
            setFooterLabel(CertificateFooter);
            setAsymmetricAlgorithmType(AsymmetricAlgorithmTypeEnum.Certificate);
        } else if (CheckHeaderAndFooter(RSAPublicKeyHeader, RSAPublicKeyFooter)) {
            setHeaderLabel(RSAPublicKeyHeader);
            setFooterLabel(RSAPublicKeyFooter);
            setAsymmetricAlgorithmType(AsymmetricAlgorithmTypeEnum.RSAPublicKey);
        } else if (CheckHeaderAndFooter(RSAPrivateKeyHeader, RSAPrivateKeyFooter)) {
            setHeaderLabel(RSAPrivateKeyHeader);
            setFooterLabel(RSAPrivateKeyFooter);
            setAsymmetricAlgorithmType(AsymmetricAlgorithmTypeEnum.RSAPrivateKey);
        } else if (CheckHeaderAndFooter(PrivateKeyHeader, PrivateKeyFooter)) {
            setHeaderLabel(PrivateKeyHeader);
            setFooterLabel(PrivateKeyFooter);
            setAsymmetricAlgorithmType(AsymmetricAlgorithmTypeEnum.Pkcs8PrivateKey);
        } else if (CheckHeaderAndFooter(PrivateEncryptedKeyHeader, PrivateEncryptedKeyFooter)) {
            setHeaderLabel(PrivateEncryptedKeyHeader);
            setFooterLabel(PrivateEncryptedKeyFooter);
            setAsymmetricAlgorithmType(AsymmetricAlgorithmTypeEnum.EncryptedPkcs8PrivateKey);
            if (StringUtils.isEmpty(secret)) {
                throw new IllegalArgumentException("privateKeyOrCetificateText can't be null");
            }

            this.secret = secret;
        } else {
            throw new InvalidKeyException("Impossible to identify the Algorithm");
        }
    }

    /**
     * Helper method to check if the a certificate, public key or private key starts
     * and ends with specified header and footer
     *
     * @param header
     * @param footer
     * @return
     */
    private boolean CheckHeaderAndFooter(String header, String footer) {
        return (getPrivateKeyOrPublicKeyOrCetificateText().startsWith(header)
                && getPrivateKeyOrPublicKeyOrCetificateText().endsWith(footer));
    }

    /**
     * Base64 string of Certificate ou Private key Or Public Key received on
     * constructor
     *
     * @return
     */
    private String getStringBase64() {
        if (StringUtils.isEmpty(_stringBase64)) {
            _stringBase64 = getPrivateKeyOrPublicKeyOrCetificateText().replace(getHeaderLabel(), "")
                    .replace(getFooterLabel(), "").replace(System.lineSeparator(), "");
        }
        return _stringBase64;

    }

    /**
     * Byte Array of Certificate ou Private key Or Public Key received on
     * constructor (Without Header and Footer)
     *
     * @return
     */
    private byte[] getSourceBytes() {
        if (_sourceBytes == null) {
            _sourceBytes = Base64.getDecoder().decode(getStringBase64());
        }
        return _sourceBytes;
    }

    /**
     * Instance of AsymmetricAlgorithm created from text passed on contructor to be
     * user to encode or decode jwt messages
     *
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws ExpiredCertificateException
     * @throws PKCSException
     * @throws OperatorCreationException
     */
    public Key getKey() throws GeneralSecurityException, ExpiredCertificateException, IOException
    {
        Key key = null;

        switch (getAsymmetricAlgorithmType()) {
            case RSAPrivateKey:
                key = getPrivateKey(getRSAPrivateKeySpec(), RSAPrivateKey.class);
                break;
            case Pkcs8PrivateKey:
                key = getPrivateKey(new PKCS8EncodedKeySpec(getSourceBytes()), RSAPrivateKey.class);
                break;
            case EncryptedPkcs8PrivateKey:
                key = getPrivateKeyFromEncryptedPrivateKey(secret);
                break;
            case RSAPublicKey:
                key = getPublicKey(getRSAPublicKeySpec(), RSAPublicKey.class);
                break;
            case PublicKey:
                key = getPublicKey(new X509EncodedKeySpec(getSourceBytes()), RSAPublicKey.class);
                break;
            case Certificate:

                X509Certificate certificate = getCertificate();
                if (certificate.getNotAfter().compareTo(new Date()) >= 0) {
                    key = certificate.getPublicKey();
                } else {
                    throw new ExpiredCertificateException(new StringBuilder().append("The certificate expired in ")
                            .append(certificate.getNotAfter()).toString());
                }
                break;
        }

        return key;
    }

    private RSAPublicKeySpec getRSAPublicKeySpec() throws IOException {
        DerInputStream der = new DerValue(getSourceBytes()).getData();
        der.getBigInteger(); // 0
        RSAPublicKeySpec pubKey = new RSAPublicKeySpec(der.getBigInteger(), // modulus
                der.getBigInteger() // publicExponent
        );

        return pubKey;
    }

    private RSAPrivateKeySpec getRSAPrivateKeySpec() throws IOException {

        DerInputStream der = new DerValue(getSourceBytes()).getData();
        der.getBigInteger(); // 0
        RSAPrivateCrtKeySpec prvKey = new RSAPrivateCrtKeySpec(der.getBigInteger(), // modulus
                der.getBigInteger(), // publicExponent
                der.getBigInteger(), // privateExponent
                der.getBigInteger(), // primeP
                der.getBigInteger(), // primeQ
                der.getBigInteger(), // primeExponentP
                der.getBigInteger(), // primeExponentQ
                der.getBigInteger() // crtCoefficient
        );

        return prvKey;
    }

    private PrivateKey getPrivateKeyFromEncryptedPrivateKey(String secret)
            throws GeneralSecurityException, IOException {
           
                
        EncryptedPrivateKeyInfo epkInfo = new EncryptedPrivateKeyInfo(getSourceBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(epkInfo.getAlgParameters().toString());
        PBEKeySpec pbeKeySpec = new PBEKeySpec(secret.toCharArray());
        SecretKey pbeKey = keyFactory.generateSecret(pbeKeySpec);
    
        Cipher cipher = Cipher.getInstance(epkInfo.getAlgParameters().toString());
        cipher.init(Cipher.DECRYPT_MODE, pbeKey, epkInfo.getAlgParameters());
        PKCS8EncodedKeySpec encodedKeySpec =  epkInfo.getKeySpec(cipher);
        return getPrivateKey(encodedKeySpec, RSAPrivateKey.class);

        
    }

    private <T extends RSAPrivateKey, K extends KeySpec> T getPrivateKey(K keySpec, Class<T> type) throws GeneralSecurityException {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return (T) kf.generatePrivate(keySpec);
    }

    private <T extends RSAPublicKey, K extends KeySpec> T getPublicKey(K keySpec, Class<T> type) throws GeneralSecurityException {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return (T) kf.generatePublic(keySpec);
    }

    private X509Certificate getCertificate() throws CertificateException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream stream = new ByteArrayInputStream(getSourceBytes());
        return (X509Certificate)cf.generateCertificate(stream);
    }


}
